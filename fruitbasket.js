const fruits_into_baskets = function(fruits) {

    var map = {};
    var k = 2;
    var start = 0;
    var maxLen = -1;

    for(var i = 0; i < fruits.length; i++) {
        var c = fruits[i];
        map[c] = map[c] ? map[c] + 1 : 1;
        var len = Object.keys(map).length;
        
        while(len > k) {
            var sChar = fruits[start];
            map[sChar] = map[sChar] - 1;
            if(map[sChar] == 0) {
                delete map[sChar];
            }
            len = Object.keys(map).length;
            start += 1;
        }
        maxLen = Math.max(maxLen, i-start+1);

    }

    return maxLen;
  };
  
  
  console.log(`Maximum number of fruits: ${fruits_into_baskets(['A', 'B', 'C', 'A', 'C'])}`)
  console.log(`Maximum number of fruits: ${fruits_into_baskets(['A', 'B', 'C', 'B', 'B', 'C'])}`)
  