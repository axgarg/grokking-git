var Heap = require("collections/heap");
class Point {

    constructor(x, y) {
      this.x = x;
      this.y = y;
    }
  
    get_point() {
      return "[" + this.x + ", " + this.y + "] ";
    }
};
  
  const find_closest_points = function(points, k) {
    var maxHeap = new Heap([], null, (a,b) => a[0]-b[0]);

    for(var  i = 0; i < points.length; i++) {
        var distance = Math.sqrt(Math.pow(points[i].x,2) + Math.pow(points[i].y,2));
        if ( maxHeap.length < k ) {
            maxHeap.push([distance, points[i]]);
        } else {
            if ( maxHeap.peek()[0] > distance ) {
                maxHeap.pop();
                maxHeap.push([distance, points[i]]);
            }
        }
    }

    var res = maxHeap.toArray();
    var result = [];
    res.forEach(element => {
        result.push(element[1]);
    });
    return result;
  };
  
  
  points = find_closest_points([new Point(1, 3), new Point(3, 4), new Point(2, -1)], 2)
  result = "Here are the k points closest the origin: ";
  for (i=0; i < points.length; i++)
    result += points[i].get_point();
  console.log(result);
  