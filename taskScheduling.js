const Deque = require('collections/deque');
const is_scheduling_possible = function(tasks, prerequisites) {
    
    var graph = Array(tasks).fill(0).map(() => Array());
    var inDegrees = Array(tasks).fill(0);

    prerequisites.forEach(element => {
        var parent = element[0];
        var child = element[1];
        graph[parent].push(child);
        inDegrees[child] = inDegrees[child] + 1;
    });

    var sources = new Deque();
    var sortedList = [];

    for(var i = 0; i < inDegrees.length; i++) {
        if(inDegrees[i] == 0) {
           sources.push(i); 
        }
    }

    while(sources.length) {
        var elem = sources.shift();
        sortedList.push(elem);
        var children = graph[elem];

        children.forEach(c => {
            inDegrees[c] = inDegrees[c] - 1;
            if(inDegrees[c] == 0) {
                sources.push(c);
            }
        });
    }

    return sortedList;
  };
  
  
  console.log(`Is scheduling possible: ${is_scheduling_possible(3, [[0, 1], [1, 2]])}`)
  console.log(`Is scheduling possible: ${is_scheduling_possible(3, [[0, 1], [1, 2], [2, 0]])}`)
  console.log(`Is scheduling possible: ${is_scheduling_possible(6, [[0, 4], [1, 4], [3, 2], [1, 3]])}`)
  