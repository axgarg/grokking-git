
const smallest_subarray_with_given_sum = function(s, arr) {

    var start = 0;
    var end = 0;
    var len = Infinity;
    var sum = 0;

    for ( var end = 0; end < arr.length; end++ ) {
        
        sum = sum + arr[end];
        
        while(sum >= s) {
            len = Math.min(len, end-start+1)
            sum = sum - arr[start]; 
            start = start + 1;
        }
    }

    if(len == Infinity) {
        return 0;
    } 

    return len;
    
  };
  
  
  console.log(`Smallest subarray length: ${smallest_subarray_with_given_sum(7, [2, 1, 5, 2, 3, 2])}`)
  console.log(`Smallest subarray length: ${smallest_subarray_with_given_sum(7, [2, 1, 5, 2, 8])}`)
  console.log(`Smallest subarray length: ${smallest_subarray_with_given_sum(8, [3, 4, 1, 1, 6])}`)