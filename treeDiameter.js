const Deque = require('collections/deque');
class TreeNode {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null; 
    }
};
  
  
class TreeDiameter {
  
    constructor() {
        this.treeDiameter = 0;
    }
  
    find_diameter(root) {
        // TODO: Write your code here
        return get_diameters(root);
    }
};

function get_diameters(root) {
    var all_dismeters = [];
    var v = get_diameters_recurse(root, new Deque(), all_dismeters );
    var max_val = 0;
    all_dismeters.forEach(v => max_val = Math.max(max_val, v))
    return max_val
}

function get_diameters_recurse(root, currentPath, all_dismeters) {

    currentPath.push(root.value);

    if(!root) {
        return;
    }

    var height = 0;

    if(!root.left && !root.right) {
        height = 1;
        all_dismeters.push(1);
    } else {
        var leftHeight = root.left ? get_diameters_recurse(root.left, currentPath, all_dismeters) : -1;
        var rightHeight = root.right ? get_diameters_recurse(root.right, currentPath, all_dismeters) : -1;
        if(leftHeight == -1 || rightHeight == -1) {

        } else {
            all_dismeters.push(1+leftHeight+rightHeight);
        }
        height = 1 + Math.max(leftHeight + rightHeight);
    }

    currentPath.pop();
    return height;
}

  
  
var treeDiameter = new TreeDiameter()
var root = new TreeNode(1)
root.left = new TreeNode(2)
root.right = new TreeNode(3)
root.left.left = new TreeNode(4)
root.right.left = new TreeNode(5)
root.right.right = new TreeNode(6)
console.log(`Tree Diameter: ${treeDiameter.find_diameter(root)}`)
root.left.left = null
root.right.left.left = new TreeNode(7)
root.right.left.right = new TreeNode(8)
root.right.right.left = new TreeNode(9)
root.right.left.right.left = new TreeNode(10)
root.right.right.left.left = new TreeNode(11)
console.log(`Tree Diameter: ${treeDiameter.find_diameter(root)}`)
  