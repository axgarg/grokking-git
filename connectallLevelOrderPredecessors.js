const Deque = require('collections/deque');
class TreeNode {

    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null; 
      this.next = null;
    }
  
    // level order traversal using 'next' pointer
    print_level_order() {
      var nextLevelRoot = this;
      while (nextLevelRoot != null) {
        var result = "";
        var current = nextLevelRoot;
        nextLevelRoot = null;
        while (current != null) {
          result += current.value + " ";
          if (nextLevelRoot != null)
            if (current.left != null)
              nextLevelRoot = current.left;
            else if (current.right != null)
              nextLevelRoot = current.right;
          current = current.next;
        }
        console.log(result);
      }
    }
  };
  
const connect_all_siblings = function(root) {
    
    var q = new Deque();
    q.push(root);
    var predecessor = null;
    var val = null;
    while(q.length) {
        var qLen = q.length;
        for(var i = 0; i < qLen; i++) {
            val = q.shift();
            if(predecessor)
                predecessor.next = val;
            predecessor = val;
            if(val.left)
                q.push(val.left);
            if(val.right)
                q.push(val.right);
        }
    }
    val.next = null;
};
  
  
  var root = new TreeNode(12);
  root.left = new TreeNode(7);
  root.right = new TreeNode(1);
  root.left.left = new TreeNode(9);
  root.right.left = new TreeNode(10);
  root.right.right = new TreeNode(5);
  connect_level_order_siblings(root);
  
  console.log("Level order traversal using 'next' pointer: ")
  root.print_level_order()
  