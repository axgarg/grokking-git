const Deque = require('collections/deque'); //http://www.collectionsjs.com

const find_subarrays = function(arr, target) {
    var result = [];
    var product = null;
    var start = 0;

    for ( var i = 0; i < arr.length; i++ ) {

        var val = arr[i];
        
        if(val < target) {
            product = !product ? val : product*val;

            while(product >= target) {
                product = product/arr[start];
                start++;
            }

            const list = new Deque();
            for ( var k = start; k <= i; k++ ) {
                list.push(arr[k]);
            }

            result.push([list.toArray()]);
            while(list.length) {
                list.shift();
                if(list.length)
                    result.push([list.toArray()]);
            }

        } else {
            start++;
        }

    }

    return result;
  };
  
  
  
console.log(`${find_subarrays([2, 5, 3, 10], 30)}`)
console.log(`${find_subarrays([8, 2, 6, 5], 50)}`)
  