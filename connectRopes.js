var Heap = require("collections/heap");
const minimum_cost_to_connect_ropes = function(ropeLengths) {
    var minHeap = new Heap([], null, (a,b) => b-a);

    for(var i = 0; i < ropeLengths.length; i++) {
        minHeap.push(ropeLengths[i]);
    }

    var firstTwo = minHeap.pop() + minHeap.pop();
    var lastVal = firstTwo;
    while ( minHeap.length ) {
        var v = minHeap.pop();
        var cost = v + lastVal;
        lastVal = cost;
        firstTwo = firstTwo + cost;
    }

    return firstTwo;
};
  
  
console.log(`Minimum cost to connect ropes: ${minimum_cost_to_connect_ropes([1, 3, 11, 5])}`)
console.log(`Minimum cost to connect ropes: ${minimum_cost_to_connect_ropes([1, 3, 11, 5, 2])}`)
