const Deque = require('collections/deque');

const print_orders = function(tasks, prerequisites) {
    
    var graph = Array(tasks).fill(0).map(() => Array());
    var sortedList = [];
    var inDegrees = Array(tasks).fill(0);

    prerequisites.forEach(element => {
        var parent = element[0];
        var child = element[1];
        graph[parent].push(child);
        inDegrees[child]++;
    });


    var sources = [];

    for(var i = 0; i < inDegrees.length; i++) {
        if(inDegrees[i] == 0) {
           sources.push(i); 
        }
    }

    find_all_orders_recursive(graph, sources, inDegrees, sortedList);

};

function find_all_orders_recursive(graph, sources, inDegrees, sortedList) {
    
    
    for(var i = 0; i < sources.length; i++) {
        var val = sources[i];
        var inDegreesClone = Object.assign([], inDegrees);
        var sourcesCloned = Object.assign([], sources);
        sourcesCloned.splice(i, 1);
        sortedList.push(val);
        graph[val].forEach(c => {
            inDegreesClone[c]--;
            if(inDegreesClone[c] == 0)
            sourcesCloned.push(c);
        });
        find_all_orders_recursive(graph, sourcesCloned, inDegreesClone, sortedList);
        if(sortedList.length == graph.length) {
            console.log(sortedList);
            sortedList = [];
        }
    }
}


// console.log(combinations_elements([1,2,3]));
  
  
// console.log("Task Orders: ")
// print_orders(3, [[0, 1], [1, 2]])
  
// console.log("Task Orders: ")
// print_orders(4, [[3, 2], [3, 0], [2, 0], [2, 1]])

// console.log("Task Orders: ")
print_orders(6, [[2, 5], [0, 5], [0, 4], [1, 4], [3, 2], [1, 3]])
// print_orders(3, [[0,2]])
  