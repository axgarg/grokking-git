const cyclic_sort = function(nums) {

    for(var i = 0; i < nums.length; i++) {
        while(nums[i]-1 != i) {
            var j = nums[i] - 1;
            [nums[i], nums[j]] = [nums[j], nums[i]]; 
        }
    }

    return nums;
  }
  
  
  console.log(`${cyclic_sort([3, 1, 5, 4, 2])}`)
  console.log(`${cyclic_sort([2, 6, 4, 3, 1, 5])}`)