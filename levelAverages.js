const Deque = require('collections/deque');
class TreeNode {

    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null; 
    }
  };
  
const find_level_averages = function(root) {
    result = [];
    var q = new Deque();
    q.push(root);

    while(q.length) {
        var qLen = q.length;
        var temp = [];
        var avg = 0;
        var count = 0;
        for (var i = 0; i < qLen; i++) {
            var val = q.shift();
            avg = avg + val.value;
            count++;
            if(val.left)
                q.push(val.left);
            if(val.right)
                q.push(val.right);
        }
        temp.push(avg/count);
        result.push(temp);
    }

    return result;
};
  
  
  var root = new TreeNode(12)
  root.left = new TreeNode(7)
  root.right = new TreeNode(1)
  root.left.left = new TreeNode(9)
  root.left.right = new TreeNode(2)
  root.right.left = new TreeNode(10)
  root.right.right = new TreeNode(5)
  
  console.log(`Level averages are: ${find_level_averages(root)}`)
  