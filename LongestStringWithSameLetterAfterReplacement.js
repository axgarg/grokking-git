const length_of_longest_substring = function(str, k) {
    
    var map = {};
    var start = 0;
    var maxLen = -1;


    for( var i = 0; i < str.length; i++ ) {
        var c = str[i];
        map[c] = map[c] ? map[c] + 1 : 1;
        var countKeys = Object.keys(map).length;

        if(countKeys > 1) {
            var highestC = 0;
            var highestCChar = "";
            var otherCount = 0;
            Object.keys(map).forEach(v => {
                if(map[v] > highestC) {
                    highestC = map[v];
                    highestCChar = v;
                }
            });

            Object.keys(map).forEach(v => {
                if(v != highestCChar) {
                    otherCount = otherCount + map[v];
                }
            });
        }

        while(otherCount > k) {
            var sChar = str[start];
            map[sChar] = map[sChar] - 1;
            if(map[sChar] == 0) {
                delete map[sChar];
            }
            var highestC = 0;
            var highestCChar = "";
            var otherCount = 0;
            Object.keys(map).forEach(v => {
                if(map[v] > highestC) {
                    highestC = map[v];
                    highestCChar = v;
                }
            });

            Object.keys(map).forEach(v => {
                if(v != highestCChar) {
                    otherCount = otherCount + map[v];
                }
            });
            start = start + 1;
        }

        maxLen = Math.max(maxLen, i-start+1);
    }

    return maxLen;
  };
  
  
  console.log(`${length_of_longest_substring("aabccbb", 2)}`)
  console.log(`${length_of_longest_substring("abbcb", 1)}`)
  console.log(`${length_of_longest_substring("abccde", 1)}`)

