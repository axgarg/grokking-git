
const triplet_sum_close_to_target = function(arr, target_sum) {
    
    arr = arr.sort((a,b) => a-b);
    var distance = Infinity;
    var triplet = null;

    for (var i = 0; i < arr.length-2; i++) {
        var r = get_pairs(arr, target_sum, arr[i], i+1, distance, triplet);
        var d = Math.abs(r[0] + r[1] + r[2] - target_sum);
        if(distance == 0) {
            return r;
        }

        if(distance > d) {
            distance = d;
            triplet = r;
        }
    }

    return triplet;
};

function get_pairs(arr, target_sum, val, start) {

    var distance = Infinity;
    var triplet = null;

    var last = arr.length-1;

    while(start < last) {
        
        var startValue = arr[start];
        var lastValue = arr[last];

        if(startValue + lastValue + val < target_sum ) {
            start++;
        } else if (startValue + lastValue + val > target_sum) {
            last--;
        } else {
            distance = 0;
            triplet = [val, startValue, lastValue];
            break;  
        }

        var tempDistance = Math.abs(lastValue + startValue + val - target_sum);
        if(tempDistance < distance) {
            distance = tempDistance;
            triplet = [val, startValue, lastValue];
        }
    }
    return triplet;

}
  
  
  
console.log(`${triplet_sum_close_to_target([-2, 0, 1, 2], 2)}`)
console.log(`${triplet_sum_close_to_target([-3, -1, 1, 2], 1)}`)
console.log(`${triplet_sum_close_to_target([1, 0, 1, 1], 100)}`)
  