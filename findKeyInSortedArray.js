const binary_search = function(arr, key) {

    var low = 0;
    var high = arr.length-1;
    var mid = Math.floor((low+high)/2);
    var isIncreasing = arr[arr.length-1] > arr[0];

    while(low <= high) {

        if(arr[mid] == key) {
            return true;
        }

        if(arr[mid] < key) {
            if(isIncreasing) {
                low = mid + 1;
            } else {
                high = mid-1;    
            }
            mid = Math.floor((low+high)/2);
        } else {

            if(isIncreasing) {
                high = mid-1;    
            } else {
                low = mid + 1;
            }
            mid = Math.floor((low+high)/2);
        }
    }
    return false;
};
  
console.log(binary_search([4, 6, 10], 10))
console.log(binary_search([1, 2, 3, 4, 5, 6, 7], 5))
console.log(binary_search([10, 6, 4], 10))
console.log(binary_search([10, 6, 5], 4))
  