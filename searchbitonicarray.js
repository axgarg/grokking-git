const search_bitonic_array = function(arr, key) {
    var low = 0;
    var high = arr.length-1;

    while(low <= high) {
        var mid = Math.floor((low+high)/2);

        if(arr[mid] == key) {
            return mid;
        }

        if(arr.length == 1) {
            return -1;
        }

        var isPeak = arr[mid-1] < arr[mid] && arr[mid] > arr[mid+1];
        
        if(isPeak) {
            var x = search_bitonic_array(arr.slice(mid+1), key); 
            var y = search_bitonic_array(arr.slice(0, mid), key);

            if(x == -1 && y == -1)
                return -1;
            if(x > -1 ) {
                return mid+1+x;
            } else if(y > -1){
                return y;
            }

        } else {
            var isIncreasing = arr[mid] > arr[mid-1] || arr[mid] < arr[mid+1]; 
            if(isIncreasing) {
                if(arr[mid] > key) {
                    high = mid-1;
                } else {
                    low = mid+1;
                }

            } else {
                if(arr[mid] > key) {
                    low = mid+1;
                } else {
                    high = mid-1;
                }
            }
        }
    }

    return -1;
};
  
  
console.log(search_bitonic_array([1, 3, 8, 4, 3], 4))
console.log(search_bitonic_array([3, 8, 3, 1], 8))
console.log(search_bitonic_array([3, 8, 3, 12], 12));
console.log(search_bitonic_array([10, 9, 8], 10))
  