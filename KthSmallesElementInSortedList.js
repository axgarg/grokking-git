var Heap = require("collections/heap");

const find_Kth_smallest = function(lists, k) {

    var minHeap = new Heap([], null, (a,b) => b[0]-a[0]);
    var indexArr = [];
    var resultArr = [];
    
    for(var i = 0; i < lists.length; i++) {
        var list = lists[i];
        indexArr.push([1, list.length-1]);
        minHeap.push([list[0], i]);
    }

    while(minHeap.length) {
        var element = minHeap.pop();
        var value = element[0];
        var index = element[1];
        if(!resultArr.peek() || resultArr[resultArr.length-1] < value)
            resultArr.push(value);

        if(resultArr.length == k) {
            break;
        }
    
        if(indexArr[index][0] <= indexArr[index][1]) {
            minHeap.push([lists[index][indexArr[index][0]], index]);
            indexArr[index][0] = indexArr[index][0] + 1;
        }
    }

    return resultArr.pop();
};
  
  
  console.log(`Kth smallest number is: ${find_Kth_smallest([[2, 6, 8], [3, 6, 7], [1, 3, 4]], 5)}`)

  
