class Node {
    constructor(value, next=null){
      this.value = value;
      this.next = next;
    }
  
    get_list() {
      var result = "";
      var temp = this;
      while (temp !== null) {
        result += temp.value + " ";
        temp = temp.next;
      }
      return result;
    }
  };
  
  const reverse_sub_list = function(head, p, q) {
    
    var count = 1;
    var oldHead = head;
    var newHaed = null;
    var newTail = null;
    var smallHead = null;

    while(head) {
        if(count == p - 1) {
            newHaed = head;
        }
        if(count == p) {
            smallHead = head;
        }

        if(count == q + 1) {
            newTail = head;
            break;
        }
        var temp = head.next; 
        if(count == q) {
            head.next = null;
        }
        head = temp;
        count++;
    }

    var nH = reverse(smallHead);
    newHaed.next = nH;
    smallHead.next = newTail;
    return oldHead;

  };
  

  function reverse(head) {
      var previous = null;
      var current = head;

      while(current) {
          var next = current.next;
          current.next = previous;
          previous = current;
          current = next;
      }
      return previous;
  }
  
  head = new Node(1)
  head.next = new Node(2)
  head.next.next = new Node(3)
  head.next.next.next = new Node(4)
  head.next.next.next.next = new Node(5)
  
  console.log(`Nodes of original LinkedList are: ${head.get_list()}`)
  console.log(`Nodes of reversed LinkedList are: ${reverse_sub_list(head, 2, 4).get_list()}`)
  