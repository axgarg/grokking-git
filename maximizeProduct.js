function max_product(nums) {

    return max_product_recurse(nums,3,0);
}


function max_product_recurse(nums, count, index) {

    if(nums.length - index < count) {
        return 0;
    }

    if(count == 0) {
        return 1;
    }

    if(nums.length - index == count) {
        var p = 1;
        for(let i = index; i < nums.length; i++) {
            p = p*nums[i];
        }

        return p;
    }

    var product1 = nums[index]*max_product_recurse(nums, count-1, index+1);
    var product2 = max_product_recurse(nums, count, index+1);

    return Math.max(product1, product2);

}



console.log(max_product([10, 3, 5, 6, 20]));
console.log(max_product([-10, -3, -5, -6, -20]));
console.log(max_product([1, -4, 3, -6, 7, 0]))


