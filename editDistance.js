


function manipulate(str1, str2, start) {

    var replace = 0;
    var remove = 0;
    var insert = 0;

    for(let i = start; i < str1.length; i++) {

        if(str1[i] != str2[i]) {
            replace = 1 + manipulate(str1, str2, i+1);
            var temp = str2.slice(0,i) + str2.slice(i+1);
            remove = 1 + manipulate(str1, temp, i);
            var temp = str2.slice(0,i) + str1[i] + str2.slice(i);
            insert = 1 + manipulate(str1, temp, i+1);
        }    
    }

    return Math.min(replace, remove, insert);    
}

console.log(manipulate("geek", "gesek", 0));

// geek
// gesek

