const find_all_duplicates = function(nums) {
    duplicateNumbers = [];
    // TODO: Write your code here

    let i = 0;
    let len = nums.length;

    while( i < len) {
        var j = nums[i] - 1;
        if(j != i && nums[i] != nums[j]) {
            [nums[i], nums[j]] = [nums[j], nums[i]];
        } else {
            i++;
        }
    }

    i = 0;

    while(i < len) {
        if(nums[i] != i + 1) {
            duplicateNumbers.push(nums[i]);
        }
        i++;
    }

    return duplicateNumbers;
  };
  
  
  console.log(`${find_all_duplicates([3, 4, 4, 5, 5])}`)
  console.log(`${find_all_duplicates([5, 4, 7, 2, 3, 5, 3])}`)