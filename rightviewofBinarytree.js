const Deque = require('collections/deque');
// const Deque = require('./collections/deque'); //http://www.collectionsjs.com
class TreeNode {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null; 
    }
};
  
const tree_right_view = function(root) {
    var result = [];
    var q = new Deque();
    q.push(root);

    while(q.length) {
        var qLen = q.length;
        for(var i = 0; i < qLen; i++) {
            val = q.shift();

            if(val.left)
                q.push(val.left);
            if(val.right)
                q.push(val.right);
            if(i == qLen-1) {
                result.push(val.value);
            }
        }
    }

    return result;
};
  
  
var root = new TreeNode(12);
root.left = new TreeNode(7);
root.right = new TreeNode(1);
root.left.left = new TreeNode(9);
root.right.left = new TreeNode(10);
root.right.right = new TreeNode(5);
root.left.left.left = new TreeNode(3);
console.log("Tree right view: " + tree_right_view(root))
  