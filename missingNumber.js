const find_missing_number = function(nums) {
    for(var i = 0; i < nums.length; i++) {
        while(nums[i] != i && nums[i] < nums.length) {
            var j = nums[i];
            [nums[i], nums[j]] = [nums[j], nums[i]];
        }
    }
    var k = 0;
    while(nums[k] == k) {
        k++;
    }
    
    return k;
};
  
  
console.log(`${find_missing_number([4, 0, 3, 1])}`)
console.log(`${find_missing_number([8, 3, 5, 2, 4, 6, 0, 1])}`)