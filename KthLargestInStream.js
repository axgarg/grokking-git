var Heap = require("collections/heap");
class KthLargestNumberInStream {
    constructor(nums, k) {
        this.minHeap = new Heap([], null, (a,b) => b-a);
        nums.forEach(element => {
            if(this.minHeap.length < k) {
                this.minHeap.push(element);
            } else {
                if(this.minHeap.peek() < element) {
                    this.minHeap.pop();
                    this.minHeap.push(element);
                }
            }
        });
        this.k = k;
        this.nums = nums;
    }
  
    add(num) {
        this.nums.push(num);
        if(this.minHeap.peek() < num) {
            this.minHeap.pop();
            this.minHeap.push(num);
        }
        // TODO: Write your code here
        return this.minHeap.peek();
    }
  };
  
  
  kthLargestNumber = new KthLargestNumberInStream([3, 1, 5, 12, 2, 11], 4);
  console.log(`4th largest number is: ${kthLargestNumber.add(6)}`)
  console.log(`4th largest number is: ${kthLargestNumber.add(13)}`)
  console.log(`4th largest number is: ${kthLargestNumber.add(4)}`)
  