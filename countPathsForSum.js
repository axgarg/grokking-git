const Deque = require('collections/deque');
class TreeNode {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null; 
    }
};
  
  
const count_paths = function(root, S) {
    var allPath = [];
    count_paths_recurse(root, new Deque(), allPath);
    var count = 0;
    allPath.forEach(v => count = count + find_all_combos_for_sum_s(v, S));
    return count;
};

function find_all_combos_for_sum_s(arr, s) {
    var temp = 0;
    var c = 0;
    var start = 0;
    for(var i = 0; i < arr.length; i++) {
        temp = temp + arr[i];
        while(temp > s) {
            temp = temp - arr[start];
            start++;
        }
        if(temp == s) {
            c++;
        }
    }
    return c;
}

function count_paths_recurse(root, currentPath, allPaths) {

    if(!root) {
        return;
    }

    currentPath.push(root.value);

    if(!root.left && !root.right) {
        allPaths.push(currentPath.toArray());
    } else {
        count_paths_recurse(root.left, currentPath, allPaths);
        count_paths_recurse(root.right, currentPath, allPaths);
    }

    currentPath.pop();
};
  
  
var root = new TreeNode(12)
root.left = new TreeNode(7)
root.right = new TreeNode(1)
root.left.left = new TreeNode(4)
root.right.left = new TreeNode(10)
root.right.right = new TreeNode(5)
console.log(`Tree has paths: ${count_paths(root, 11)}`)
  