const non_repeat_substring = function(str) {
    // TODO: Write your code here
    var map = {};
    var start = 0;
    var maxLen = -1;

    for(var i = 0; i < str.length; i++) {
        var c = str[i];
        if(map[c]) {
            start = map[c] + 1;
            delete map[c];
        }
        map[c] = i;
        maxLen = Math.max(maxLen, i-start+1);
    }

    return maxLen;
  };
  
  
  console.log(`Length of the longest substring: ${non_repeat_substring("aabccbb")}`)
  console.log(`Length of the longest substring: ${non_repeat_substring("abbbb")}`)
  console.log(`Length of the longest substring: ${non_repeat_substring("abccde")}`)