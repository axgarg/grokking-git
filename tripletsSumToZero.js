const search_triplets = function(arr) {

    arr = arr.sort((a,b) => a-b);
    var triplets = [];

    for(var  i = 0; i < arr.length; i++) {
        if(arr[i] == arr[i-1] && i > 0) {
            continue;
        }
        search_pair(arr, -arr[i], i+1, triplets);
    }
    return triplets;
};

function search_pair(arr, targetSum, left, triplets) {
    var right = arr.length-1;

    while(left < right) {
        var leftValue = arr[left];
        var rightValue = arr[right];
        if(leftValue + rightValue == targetSum) {
            triplets.push([-targetSum, leftValue, rightValue]);
            left++;
            right--;
            while(left < right && arr[left] == arr[left-1]) {
                left++;
            }
            while(left < right && arr[right] == arr[right+1]) {
                right--;
            }
        } else if(leftValue+rightValue < targetSum) {
            left++;
        } else {
            right--;
        }
    }
}
  
  
console.log(`${search_triplets([-3, 0, 1, 2, -1, 1, -2])}`)
console.log(`${search_triplets([-5, 2, -1, -2, 3])}`)
  