const find_subsets = function(nums) {
    var subsets = [];
    var previousStep = null;
    var previousElem = null;

    nums = nums.sort((a,b) => a-b);
    subsets.push([]);

    nums.forEach(element => {

        var iterArr = [];
        if(element == previousElem) {
            iterArr = previousStep;
        } else {
            iterArr = subsets;
        }

        var temp = [];
        iterArr.forEach(s => {
            var n = Object.assign([], s);
            n.push(element);
            temp.push(n);
            subsets.push(n);
        });
        previousElem = element;
        previousStep = temp;
    });

    return subsets;
};
  
  
console.log('Here is the list of subsets: ');
let result = find_subsets([1, 3, 3]);
result.forEach((subset) => {
  console.log(subset);
});

console.log('Here is the list of subsets: ');
result = find_subsets([1, 5, 3, 3]);
result.forEach((subset) => {
  console.log(subset);
});
  