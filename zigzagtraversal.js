const Deque = require('collections/deque');
class TreeNode {

    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null; 
    }
  };
  
  
const traverse = function(root) {
    var result = [];
    // TODO: Write your code here
    var q = new Deque();
    q.push(root);
    var rightToLeft = false;

    while(q.length) {
        var qLen = q.length;
        var temp = new Deque();
        for ( var i = 0; i < qLen; i++ ) {
            var val = q.shift();
            if(rightToLeft)
                temp.unshift(val.value);
            else
                temp.push(val.value);
            if(val.left)
                q.push(val.left);
            if(val.right)
                q.push(val.right);
        }
        rightToLeft = rightToLeft ? false : true;
        result.push(temp.toArray());
    }
    return result;
};
  
  
  var root = new TreeNode(12)
  root.left = new TreeNode(7)
  root.right = new TreeNode(1)
  root.left.left = new TreeNode(9)
  root.right.left = new TreeNode(10)
  root.right.right = new TreeNode(5)
  root.right.left.left = new TreeNode(20)
  root.right.left.right = new TreeNode(17)
  console.log(`Zigzag traversal: ${traverse(root)}`)  