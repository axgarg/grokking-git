class TreeNode {

    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null; 
    }
  };
  
  
// const find_sum_of_path_numbers = function(root) {
//     var allNums = [];
//     find_sum_of_path_numbers_recursive(root, [], '0', allNums);
//     var sum = 0;
//     allNums.forEach(v => sum = parseInt(v) + sum);
//     return sum;

// };

// function find_sum_of_path_numbers_recursive(root, currentPath, num, allNums) {
    
//     if(!root) {
//         return;
//     }

//     currentPath.push(root.value);
//     num = num + root.value;

//     if(!root.left && !root.right) {
//         allNums.push(num);
//     } else {
//         find_sum_of_path_numbers_recursive(root.left, currentPath, num, allNums);
//         find_sum_of_path_numbers_recursive(root.right, currentPath, num, allNums);
//     }
//     currentPath.pop();
// }

const find_sum_of_path_numbers = function(root) {
    return find_sum_of_path_numbers_recursive(root, '0');
    // return -1;
};

function find_sum_of_path_numbers_recursive(root, num) {
    
    num = num + root.value;
    if(!root.left && !root.right) {
        return num;
    }

    var lVal = root.left ? find_sum_of_path_numbers_recursive(root.left, num) : 0;
    var rVal = root.right ? find_sum_of_path_numbers_recursive(root.right, num) : 0;

    return (parseInt(rVal) + parseInt(lVal)).toString();

}


  
  
  
var root = new TreeNode(1)
root.left = new TreeNode(0)
root.right = new TreeNode(1)
root.left.left = new TreeNode(1)
root.right.left = new TreeNode(6)
root.right.right = new TreeNode(5)
console.log(`Total Sum of Path Numbers: ${find_sum_of_path_numbers(root)}`)
  