const Deque = require('collections/deque');

class TreeNode {

    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null; 
    }
};
  
  
const traverse = function(root) {
    var result = [];
    var queue = new Deque();
    queue.push(root);
    while(queue.length) {
        var queueLen = queue.length;
        var temp = [];
        for(var i = 0; i < queueLen; i++) {
            var v = queue.shift();
            temp.push(v.value);
            if(v.left)
                queue.push(v.left);
            if(v.right)
                queue.push(v.right);
        }
        result.push(temp);
    }

    return result;
};
  
  
  
var root = new TreeNode(12);
root.left = new TreeNode(7);
root.right = new TreeNode(1);
root.left.left = new TreeNode(9);
root.right.left = new TreeNode(10);
root.right.right = new TreeNode(5);
console.log(`Level order traversal: ${traverse(root)}`);
