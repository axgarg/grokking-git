class TreeNode {

    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null; 
    }
};
  
  
  
const find_maximum_path_sum = function(root) {
    // TODO: Write your code here
    var allValues = [];
    find_max_sum(root, allValues);
    console.log(allValues);
    var max_val = null;
    allValues.forEach(v => {
        if(!max_val)
            max_val = v;
        else
            max_val = Math.max(max_val, v);
    });
    return max_val;
};

function find_max_sum(root, allValues) {

    if(!root) {
        return null;
    }

    if(!root.left && !root.right) {
        allValues.push(root.value);
        return root.value;
    }

    var left_sum = find_max_sum(root.left, allValues);
    var right_sum = find_max_sum(root.right, allValues);
    if(!left_sum || !right_sum){
        
    } else {
        allValues.push(root.value + left_sum + right_sum);
    }

    return root.value + Math.max(left_sum, right_sum);
}
  
  
  
var root = new TreeNode(1)
root.left = new TreeNode(2)
root.right = new TreeNode(3)
console.log(`Maximum Path Sum: ${find_maximum_path_sum(root)}`)

root.left.left = new TreeNode(1)
root.left.right = new TreeNode(3)
root.right.left = new TreeNode(5)
root.right.right = new TreeNode(6)
root.right.left.left = new TreeNode(7)
root.right.left.right = new TreeNode(8)
root.right.right.left = new TreeNode(9)
console.log(`Maximum Path Sum: ${find_maximum_path_sum(root)}`)

root = new TreeNode(-1)
root.left = new TreeNode(-3)
console.log(`Maximum Path Sum: ${find_maximum_path_sum(root)}`)
  