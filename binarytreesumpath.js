class TreeNode {

    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null; 
    }
};
  
  
const has_path = function(root, sum) {
    // TODO: Write your code here
    if(root.value == sum && !root.left && !root.right) {
        return true;
    }

    var has_left_path = root.left ? has_path(root.left, sum - root.value) : false;
    var has_right_path = root.right ? has_path(root.right, sum - root.value) : false;

    return has_left_path || has_right_path;
};
  
  
  var root = new TreeNode(12)
  root.left = new TreeNode(7)
  root.right = new TreeNode(1)
  root.left.left = new TreeNode(9)
  root.right.left = new TreeNode(10)
  root.right.right = new TreeNode(5)
  console.log(`Tree has path: ${has_path(root, 23)}`)
  console.log(`Tree has path: ${has_path(root, 16)}`)
  