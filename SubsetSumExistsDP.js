
function min_sum(nums) {

    var sum = 0;
    var minElem = Infinity; 
    nums.forEach(element => {
        sum = sum + element;
        minElem = Math.min(minElem, element);
    });

    var maxDiff = sum - minElem;
    var dp = [];

    for(var i = 0; i <= maxDiff; i++ ) {
        if(i == 0 && sum%2 != 0) {
            continue;
        }
        var findSum = parseInt(sum/2) - i;
        var result = find_set_withsum(nums, findSum, 0, dp);
        if (result) return sum-2*findSum;
    }

    return maxDiff;
}

function find_set_withsum(nums, sum, index, dp) {

    dp[index] = dp[index] || [];
    if(dp[index][sum])
        return dp[index][sum];
    
    if(index > nums.length - 1) {
        dp[index][sum] = false;
        return false;
    }

    var p1 = false;
    if(sum-nums[index] > 0)
        p1 = find_set_withsum(nums, sum - nums[index], index+1, dp);
    else if(sum - nums[index] == 0) {
        dp[index][sum] = true;
        return true;
    }
    var p2 = find_set_withsum(nums, sum, index+1, dp);

    var res = p1 || p2;
    dp[index][sum] = res;
    return res;
}

console.log(`Minimum subset difference is: ---> ${min_sum([1, 2, 3, 9])}`);
console.log(`Minimum subset difference is: ---> ${min_sum([1, 2, 7, 1, 5])}`);
console.log(`Minimum subset difference is: ---> ${min_sum([1, 3, 100, 4])}`);