const find_duplicate = function(nums) {
    // TODO: Write your code here

    let i = 0;
    let len = nums.length;

    while(i < len) {
        var j = nums[i] - 1;
        if(j != i) {
            if(nums[j] == nums[i]) {
                return nums[j];
            } else {
                [nums[i], nums[j]] = [nums[j], nums[i]]; 
            }
        } else {
            i++;
        }
    }


    return -1;
  };
  
  
  console.log(`${find_duplicate([1, 4, 4, 3, 2])}`)
  console.log(`${find_duplicate([2, 1, 3, 3, 5, 4])}`)
  console.log(`${find_duplicate([2, 4, 1, 4, 4])}`)