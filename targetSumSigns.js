const findTargetSubsets = function(nums, s) {
    // TODO: Write your code here 
    return findTargetSubsetsRecursive(nums, s, 0);
    // return -1;
  };
  

function findTargetSubsetsRecursive(nums, s, index) {

    if(index == nums.length-1) {
        var isPlus = s + nums[index] == 0 ? 1 : 0;
        var isMinus = s - nums[index] == 0 ? 1 : 0;
        return isPlus + isMinus;
    }

    var ifPlus = findTargetSubsetsRecursive(nums, s - nums[index], index+1);
    var ifMinus = findTargetSubsetsRecursive(nums, s + nums[index], index+1);
    
    return ifMinus + ifPlus;
    
}
  
// console.log(`Count of Target sum is: ---> ${findTargetSubsets([2, 3, 1], 5)}`);
console.log(`Count of Target sum is: ---> ${findTargetSubsets([1, 1, 2, 3], 1)}`);
console.log(`Count of Target sum is: ---> ${findTargetSubsets([1, 2, 7, 1], 9)}`);
  