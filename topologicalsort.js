const Deque = require('collections/deque');

const topological_sort = function(vertices, edges) {
    var sortedOrder = [];
    
    //a. initialize the graph
    var graph = Array(vertices).fill(0).map(() => Array());
    var inDegree = Array(vertices).fill(0);

    edges.forEach(element => {
        var parent = element[0];
        var child = element[1];
        graph[parent].push(child);
        inDegree[child] = inDegree[child] + 1; 
    });

    var q = new Deque();

    for(var  i = 0; i < inDegree.length; i++) {
        var element = inDegree[i];
        if (element == 0) {
            q.push(i);
        }
    }

    while(q.length) {
        var val = q.shift();
        sortedOrder.push(val);

        graph[val].forEach(c => {
            inDegree[c] = inDegree[c] - 1;
            if(inDegree[c] == 0) {
                q.push(c);
            } 
        });
    }

    return sortedOrder;
};
  
  
console.log(`Topological sort: ${
    topological_sort(4, [
      [3, 2],
      [3, 0],
      [2, 0],
      [2, 1],
    ])}`);
  
  console.log(`Topological sort: ${
    topological_sort(5, [
      [4, 2],
      [4, 3],
      [2, 0],
      [2, 1],
      [3, 1],
    ])}`);
  console.log(`Topological sort: ${
    topological_sort(7, [
      [6, 4],
      [6, 2],
      [5, 3],
      [5, 4],
      [3, 0],
      [3, 1],
      [3, 2],
      [4, 1],
    ])}`);