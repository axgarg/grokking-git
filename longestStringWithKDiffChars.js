const longest_substring_with_k_distinct = function(str, k) {
    var map = {};
    var start = 0;
    var minLength = -1;

    for(var i = 0; i < str.length; i++) {
        var char = str[i];
        map[char] = map[char] ?  map[char] + 1 : 1;
        var count = Object.keys(map).length;

        while(count > k) {
            var sChar = str[start];
            map[sChar] = map[sChar] - 1;
            if(map[sChar] == 0) {
                delete map[sChar];
            } 
            count = Object.keys(map).length;
            start = start + 1;
        }
        minLength = Math.max(minLength,i-start+1);

    }

    return minLength;
};
  
  
console.log(`Length of the longest substring: ${longest_substring_with_k_distinct("araaci", 2)}`)
console.log(`Length of the longest substring: ${longest_substring_with_k_distinct("araaci", 1)}`)
console.log(`Length of the longest substring: ${longest_substring_with_k_distinct("cbbebi", 3)}`)