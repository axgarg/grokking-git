const find_permutations = function(nums) {

    var result = [];
    var temp = [];
    temp.push([]);
    var len = nums.length;

    nums.forEach(element => {
        temp.forEach(val => {
            var x = Object.assign([], val);
            var tempLen = x.length;
            if(tempLen == 0) {
                x.push(element);
                temp.push(x);
            } else {
                for(var k = 0; k <= tempLen; k++) {
                    var y = Object.assign([], x);
                    y.splice(k,0,element);
                    temp.push(y);
                }
            }
        });
    });
    temp.forEach(elem => {
        if(elem.length == len) {
            result.push(elem);
        }
    });
    return result;
};

// console.log(`Here are all the permutations: ${find_permutations([1, 3, 5])}`);
console.log('Here are all the permutations:');
const result = find_permutations([1, 3, 5]);
result.forEach((permutation) => {
  console.log(permutation);
});

  