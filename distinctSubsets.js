const find_subsets = function(nums) {
    var subsets = [];
    subsets.push([]);

    nums.forEach(element => {
        subsets.forEach(s => {
            var n = Object.assign([], s);
            n.push(element);
            subsets.push(n);
        });
    });
    
    return subsets;
  };
  
  
  console.log('Here is the list of subsets: ');
  let result = find_subsets([1, 3]);
  result.forEach((subset) => {
    console.log(subset);
  });
  
  console.log('Here is the list of subsets: ');
  result = find_subsets([1, 5, 3]);
  result.forEach((subset) => {
    console.log(subset);
  });
  