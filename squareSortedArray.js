const make_squares = function(arr) {
    var arrLen = arr.length;
    var squares = Array(arrLen).fill(0);
    var smallPointer = 0;
    var bigPointer = arrLen - 1;
    var currentIndex = bigPointer;

    while(smallPointer <= bigPointer) {
        let leftSquare = arr[smallPointer]*arr[smallPointer];
        let rightSquare = arr[bigPointer]*arr[bigPointer];
        if(leftSquare > rightSquare) {
            squares[currentIndex] = leftSquare;
            smallPointer++;
        } else {
            squares[currentIndex] = rightSquare;
            bigPointer--;
        }
        currentIndex--;
    }
    return squares;
};
  
  
console.log(`Squares: ${make_squares([-2, -1, 0, 2, 3])}`)
console.log(`Squares: ${make_squares([-3, -1, 0, 1, 2])}`)
  