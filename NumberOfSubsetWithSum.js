const countSubsets = function(nums, sum) {
    var dp = [];
    var res = countSubsetsRecursive(nums, sum, 0, dp);
    // console.log(dp);
    return res;
};

function countSubsetsRecursive(nums, sum, index, dp) {

    if(index > nums.length-1) {
        return 0;
    }

    var isAvailable1 = 0;

    if(sum-nums[index] > 0)
        isAvailable1 = countSubsetsRecursive(nums, sum-nums[index], index+1, dp);
    else if(sum-nums[index] == 0) {
        isAvailable1 = 1;
    }

    var isAvailable2 = countSubsetsRecursive(nums, sum, index+1, dp);
    
    return isAvailable1 + isAvailable2;
}

console.log(`Count of subset sum is: ---> ${countSubsets([1, 1, 2, 3], 5)}`);
console.log(`Count of subset sum is: ---> ${countSubsets([1, 2, 7, 1, 5], 9)}`);