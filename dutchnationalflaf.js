
const dutch_flag_sort = function(arr) {
    // TODO: Write your code here
    var low = 0;
    var high = arr.length - 1;

    for(var i = 0; i < arr.length; i++) {
        var val = arr[i];
        if(val == 0) {
            if(i > low) {
                [arr[i], arr[low]] = [arr[low], arr[i]];
                low++;
                i--;
            }
        } else if(val == 2 && i < arr.length-1) {
            if(i < high) {
                [arr[i], arr[high]] = [arr[high], arr[i]];
                high--;
                i--;
            }
        }
    }
    return arr;
};
  
  
arr = [1, 0, 2, 1, 0];
dutch_flag_sort(arr);
console.log(`${arr}`)
  
arr = [2, 2, 0, 1, 2, 0]
dutch_flag_sort(arr)
console.log(`${arr}`)
  