var Heap = require("collections/heap");

class ListNode {
    constructor(value, next=null){
        this.value = value;
        this.next = next;
    }
}


const merge_lists = function(lists) {

    var resultHead = null;
    var minHeap = new Heap([], null, (a,b) => b[0].value-a[0].value);
    var head = null;

    for(var i = 0; i < lists.length; i++) {
        var list = lists[i];
        if(list)
            minHeap.push([lists[i], i]);
        else
            continue;
        lists[i] = lists[i].next;
    }

    while(minHeap.length) {
        var element = minHeap.pop();
        var value = element[0];
        var index = element[1];

        if(!resultHead) {
            resultHead = value;
            head = value;

        } else {
            resultHead.next = value;
            resultHead = resultHead.next;
        }

        if(lists[index]) {
            minHeap.push([lists[index], index]);
            lists[index] = lists[index].next;
        }
    }
    
    return head;
};
  
  
  
l1 = new ListNode(2)
l1.next = new ListNode(6)
l1.next.next = new ListNode(8)

l2 = new ListNode(3)
l2.next = new ListNode(6)
l2.next.next = new ListNode(7)

l3 = new ListNode(1)
l3.next = new ListNode(3)
l3.next.next = new ListNode(4)

result = merge_lists([l1, l2, l3])
output = "Here are the elements form the merged list: ";
while (result != null) {
    output += result.value + " ";
    result = result.next;
}
console.log(output);
  