const find_max_in_bitonic_array = function(arr) {

    var low = 0;
    var high = arr.length - 1;

    while(low <= high) {

        var mid = Math.floor((high+low)/2);
        var leftValue = arr[mid-1];
        var rightValue = arr[mid+1];
        var midValue = arr[mid];

        if(leftValue < midValue && midValue > rightValue) {
            low = mid;
            break;
        }

        var isIncreasing = arr[mid] > arr[mid-1];

        if(isIncreasing)
            low = mid+1;
        else 
            high = mid-1;
    }

    return arr[low] || arr[arr.length-1];
};
  
  
console.log(find_max_in_bitonic_array([1, 3, 8, 12, 4, 2]))
console.log(find_max_in_bitonic_array([3, 8, 3, 1]))
console.log(find_max_in_bitonic_array([1, 3, 8, 12]))
console.log(find_max_in_bitonic_array([10, 9, 8]))
  