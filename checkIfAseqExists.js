class TreeNode {

    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null; 
    }
  };
  
  
  
const find_path = function(root, sequence) {
    // TODO: Write your code here
    return check_sequence(root, sequence, 0);
};

function check_sequence(root, sequence, index) {
    if(!root) {
        return false;
    }

    if(root.value != sequence[index]) {
        return false;
    }

    if (root.value == sequence[index] && !root.left && !root.right) {
        return true;
    }

    return check_sequence(root.left, sequence, index+1) || check_sequence(root.right, sequence, index+1);;
}
  
  

var root = new TreeNode(1)
root.left = new TreeNode(0)
root.right = new TreeNode(1)
root.left.left = new TreeNode(1)
root.right.left = new TreeNode(6)
root.right.right = new TreeNode(5)

console.log(`Tree has path sequence: ${find_path(root, [1, 0, 7])}`)
console.log(`Tree has path sequence: ${find_path(root, [1, 1, 6])}`)
  