var Heap = require("collections/heap");
const find_Kth_smallest_number = function(nums, k) {
    var maxHeap = new Heap([], null, (a,b) => a-b);

    for(var i = 0; i < nums.length; i++) {
        var val = nums[i];
        if(maxHeap.length < k) {
            maxHeap.push(val);
        } else {
            if(maxHeap.peek() > val) {
                maxHeap.pop();
                maxHeap.push(val);
            }
        }
    }

    return maxHeap.pop();
};
  
  
console.log(`Kth smallest number is: ${find_Kth_smallest_number([1, 5, 12, 2, 11, 5], 3)}`)
// since there are two 5s in the input array, our 3rd and 4th smallest numbers should be a '5'
console.log(`Kth smallest number is: ${find_Kth_smallest_number([1, 5, 12, 2, 11, 5], 4)}`)
console.log(`Kth smallest number is: ${find_Kth_smallest_number([5, 12, 11, -1, 12], 3)}`)
  