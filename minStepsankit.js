function steps(num) {
    if(num == 1 || num == 2 || num ==3) {
        return 1;
    }

    var minstepsFor1 = 1 + steps(num-1);
    var minstepsFor2 = num%2 == 0 ? 1 + steps(num/2) : Infinity;
    var minstepsFor3 = num%3 == 0 ? 1 + steps(num/3) : Infinity;

    return Math.min(minstepsFor1, minstepsFor2, minstepsFor3);
}

console.log(steps(5));