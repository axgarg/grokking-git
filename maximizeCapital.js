var Heap = require("collections/heap");

const find_maximum_capital = function(capital, profits, numberOfProjects, initialCapital) {
    //TODO: Write your code here

    var minHeap = new Heap([], null, ((a,b) => b[0] - a[0]));
    var maxHeap = new Heap([], null, ((a,b) => a[1] - b[1]));
    for(var i = 0; i < capital.length; i++) {
        minHeap.push([capital[i], profits[i]]);
    }
    while(numberOfProjects) {

        while(minHeap.length > 0 && minHeap.peek()[0] <= initialCapital) {
            maxHeap.push(minHeap.pop());
        }
    
        if(maxHeap.length > 0 && numberOfProjects) {
            initialCapital = maxHeap.pop()[1] + initialCapital;
            numberOfProjects--; 
        } 
    }

    return initialCapital;
  };
  
  
  console.log(`Maximum capital: ${find_maximum_capital([0, 1, 2], [1, 2, 3], 2, 1)}`)
  console.log(`Maximum capital: ${find_maximum_capital([0, 1, 2, 3], [1, 2, 3, 5], 3, 0)}`)
  