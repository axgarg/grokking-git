const Deque = require('collections/deque');
class TreeNode {

    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null; 
    }
  };
  
  
// const find_minimum_depth = function(root) {
//     if(!root) {
//         return 0;
//     }
//     var minDepth = 1 + Math.min(find_minimum_depth(root.left), find_minimum_depth(root.right));
//     return minDepth;
// };

const find_minimum_depth = function(root) {
    
    var q = new Deque();
    q.push(root);
    if(!root)
        return 0;

    var depth = 1;

    while(q.length) {
        var qLen = q.length;
        for (var i = 0; i < qLen; i++) {
            var val = q.shift();
            if(!val.left || !val.right)
                return depth;
            q.push(val.left);
            q.push(val.right);
        }
        depth++;
    }

    return depth;

};


  
  
  
  var root = new TreeNode(12)
  root.left = new TreeNode(7)
  root.right = new TreeNode(1)
  root.right.left = new TreeNode(10)
  root.right.right = new TreeNode(5)
  console.log(`Tree Minimum Depth: ${find_minimum_depth(root)}`)
  root.left.left = new TreeNode(9)
  root.right.left.left = new TreeNode(11)
  console.log(`Tree Minimum Depth: ${find_minimum_depth(root)}`)
  