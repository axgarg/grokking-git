const find_range = function(arr, key) {

    var low = 0;
    var high = arr.length-1;
    var values = [];

    find_range_recursion(arr, key, low, high, values);

    if(values.length == 0) {
        return [-1,-1];
    } else {
        values.sort((a,b) => a-b);
        return [values[0], values[values.length-1]];
    }
};

function find_range_recursion(arr, key, start, end, values) {

    var mid = Math.floor((start+end)/2);

    while(start <= end) {

        if (arr[mid] == key) {
            values.push(mid);
            find_range_recursion(arr,key,mid+1, end, values);
            find_range_recursion(arr,key,start, mid-1, values);
        }
        if(arr[mid] < key) {
            start = mid + 1;
            mid = Math.floor((start+end)/2);
        } else {
            end = mid - 1;    
            mid = Math.floor((start+end)/2);
        }
    }
}
  
  
console.log(find_range([4, 6, 6, 6, 9], 6))
console.log(find_range([1, 3, 8, 10, 15], 10))
console.log(find_range([1, 3, 8, 10, 15], 12))
  