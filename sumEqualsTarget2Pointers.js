const pair_with_targetsum = function(arr, target_sum) {
    // TODO: Write your code here
    var start = 0;
    var end = arr.length - 1;
    var loop = true;

    while(loop && start < end) {
        var small = arr[start];
        var big = arr[end];
        if(small + big == target_sum) {
            loop = false;
            break;
        } else if(small + big > target_sum) {
            end = end - 1;
        } else {
            start = start + 1;
        }
    }

    if(!loop)
        return [start, end];
    else
        return [-1, -1];
  }
  
  
console.log(`${pair_with_targetsum([1, 2, 3, 4, 6], 6)}`)
console.log(`${pair_with_targetsum([2, 5, 9, 11], 11)}`)

  
