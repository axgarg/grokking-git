const find_missing_numbers = function(nums) {
    missingNumbers = [];
    // TODO: Write your code here
    let i = 0;
    let len = nums.length;

    while(i < len) {
        var j = nums[i] - 1;
        if(j != i && nums[j] != nums[i]) {
            [nums[i], nums[j]] = [nums[j], nums[i]];
        } else{
            i += 1;
        }
    }
    i = 0;
    while(i < len) {
        if(nums[i] != i+1) {
            missingNumbers.push(i+1);
        }
        i++;
    }

    return missingNumbers;
  };
  
  
  console.log(`${find_missing_numbers([2, 3, 1, 8, 2, 3, 5, 1])}`)
  console.log(`${find_missing_numbers([2, 4, 1, 2])}`)
  console.log(`${find_missing_numbers([2, 3, 2, 1])}`)