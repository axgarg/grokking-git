let solveKnapsack = function(profits, weights, capacity) {

    return recurse(profits, weights, capacity, 0);
};

// function recurse(profits, weights, capacity) {
    
//     var maxProfit = 0;

//     if(capacity == 0) {
//         return maxProfit;
//     }

//     for(var i = 0; i < weights.length; i++) {
//         var profitsCloned = Object.assign([], profits);
//         var weightsCloned = Object.assign([], weights);
//         var reducedCapacity = capacity - weights[i];
//         var pro = 0;
//         if(reducedCapacity >= 0 ) {
//             var p = profitsCloned[i];
//             profitsCloned.splice(i,1);
//             weightsCloned.splice(i,1)
//             pro = Math.max(p + recurse(profitsCloned, weightsCloned, reducedCapacity), recurse(profitsCloned, weightsCloned, capacity));
//         }

//         maxProfit = Math.max(pro, maxProfit);
//     }

//     return maxProfit;
// }

const dp = [];

function recurse(profits, weights, capacity, index) {

    dp[index] = dp[index] || []; 

    if(dp[index][capacity])
        return dp[index][capacity];

    if(index > profits.length-1) {
        return 0;
    }

    if(capacity <= 0) {
        return 0;
    }

    var profit1 = 0;

    if(weights[index] <= capacity)
        profit1 = profits[index] + recurse(profits, weights, capacity - weights[index], index+1);

    var profit2 = recurse(profits, weights, capacity, index+1);
    
    dp[index][capacity] = Math.max(profit1, profit2);
    return dp[index][capacity];
}




var profits = [1, 6, 10, 16];
var weights = [1, 2, 3, 5];

console.log(`Total knapsack profit: ---> ${solveKnapsack(profits, weights, 7)}`);
console.log(`Total knapsack profit: ---> ${solveKnapsack(profits, weights, 6)}`);