const can_partition = function(num) {
    
    var dp = [];
    var sum = 0;
    num.forEach(element => {
        sum = sum + element; 
    });

    if(sum%2 != 0)
        return false;

    var findSum = sum/2;
    return recurse(num, findSum, 0, dp);

    // return false;
};
  

function recurse(num, sum, index, dp) {

    dp[index] = dp[index] || [];
    if(dp[index][sum])
        return dp[index][sum];


    if(index > num.length-1) {
        return false;
    }

    var possibility1 = false;
    if(sum-num[index] > 0)
        possibility1 = recurse(num, sum-num[index], index+1, dp);
    else if(sum-num[index] == 0) {
        dp[index][sum] = true;
        return true;
    }


    var possibility2 = recurse(num, sum, index+1, dp);
    var res = possibility1 || possibility2;
    dp[index][sum] = res;
    return dp[index][sum];
}


console.log(`Can partition: ${can_partition([1, 2, 3, 4])}`)
console.log(`Can partition: ${can_partition([1, 1, 3, 4, 7])}`)
console.log(`Can partition: ${can_partition([2,5,4,9])}`)
  