class Node {
    constructor(value, next=null){
      this.value = value;
      this.next = next;
    }
  
    get_list() {
      var result = "";
      var temp = this;
      while (temp !== null) {
        result += temp.value + " ";
        temp = temp.next;
      }
      return result;
    }
  };
  
  
  
const reverse_every_k_elements = function(head, k) {
// TODO: Write your code here
    var count = 1;
    var oldHead = null;
    var currentHead = head;
    var newLLStart = null;
    var newLLEnd = null;


    while(head) {

        if (count == k) {

            var temp = head.next;
            head.next = null;
            var oHead = reverse(currentHead);
            if(!newLLStart)
                newLLStart = oHead;
            if(!newLLEnd) {
                newLLEnd = currentHead;
            } else {
                newLLEnd.next = oHead;
                newLLEnd = currentHead;
            }
            currentHead = temp;
            head = temp;
            count = 1;

        } else {
            head = head.next;
            count++;
        }
    }

    var oHead = reverse(currentHead);
    if(!newLLStart)
        newLLStart = oHead;

    if(!newLLEnd) {
        newLLEnd = currentHead;
    } else {
        newLLEnd.next = oHead;
        newLLEnd = currentHead;
    }

    return newLLStart;
}

function reverse(head) {
    var previous = null;
    var current = head;

    while(current) {
        var next = current.next;
        current.next = previous;
        previous = current;
        current = next;
    }
    return previous;
}
  
  
  head = new Node(1)
  head.next = new Node(2)
  head.next.next = new Node(3)
  head.next.next.next = new Node(4)
  head.next.next.next.next = new Node(5)
  head.next.next.next.next.next = new Node(6)
  head.next.next.next.next.next.next = new Node(7)
  head.next.next.next.next.next.next.next = new Node(8)
  
  console.log(`Nodes of original LinkedList are: ${head.get_list()}`)
  console.log(`Nodes of reversed LinkedList are: ${reverse_every_k_elements(head, 3).get_list()}`)
  