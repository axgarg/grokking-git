const search_ceiling_of_a_number = function(arr, key) {

    var low = 0;
    var high = arr.length-1;
    var mid = Math.floor((low+high)/2);

    while(low <= high) {

        if (arr[mid] == key) {
            return mid;
        }

        if(arr[mid] < key) {
            low = mid + 1;
            mid = Math.floor((low+high)/2);
        } else {
            high = mid - 1;    
            mid = Math.floor((low+high)/2);
        }
    }

    return low;

};
  
  
console.log(search_ceiling_of_a_number([4, 6, 10], 10))
console.log(search_ceiling_of_a_number([1, 3, 8, 10, 15], 12))
console.log(search_ceiling_of_a_number([4, 6, 10], 17))
console.log(search_ceiling_of_a_number([4, 6, 10], -1))
  