var Heap = require("collections/heap");

class Interval {
    constructor(start, end) {
      this.start = start;
      this.end = end;
    }
};
  
  
  const find_next_interval = function(intervals) {
    result = [];
    intervals.forEach(k => result.push(-1));

    var maxHeapStart = new Heap([], null, ((a,b) => a[0].start - b[0].start));
    var maxHeapEnd = new Heap([], null, ((a,b) => a[0].end - b[0].end));


    for ( var i = 0; i < intervals.length; i++ ) {
        maxHeapStart.push([intervals[i], i]);
        maxHeapEnd.push([intervals[i], i]);
    }

    while(maxHeapEnd.length > 0) {
        var val = maxHeapEnd.pop();
        var valIndex = val[1];
        var index = -1;
        var poppedElems = [];
        while(maxHeapStart.peek()[0].start >= val[0].end) {
            var popelem = maxHeapStart.pop();
            index = popelem[1];
            poppedElems.push(popelem);
        }
        poppedElems.forEach(p => maxHeapStart.push(p));
        result[valIndex] = index;
    }

    return result;

  };
  
  
  result = find_next_interval(
    [new Interval(2, 3), new Interval(3, 4), new Interval(5, 6)])
  console.log(`Next interval indices are: ${result}`)
  
  
  result = find_next_interval(
    [new Interval(3, 4), new Interval(1, 5), new Interval(4, 6)])
  console.log(`Next interval indices are: ${result}`)
  