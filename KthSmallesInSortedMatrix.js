var Heap = require("collections/heap");

const find_Kth_smallest = function(matrix, k) {
    var minHeap = new Heap([], null, (a,b) => b[0] - a[0]);
    var indexArr = [];
    var result = [];

    for(var  i = 0; i < matrix.length; i++) {
        var list = matrix[i];
        indexArr[i] = [1, list.length-1];
        minHeap.push([list[0], i]);
    }

    while(minHeap.length) {

        var element = minHeap.pop();
        var value = element[0];
        var index = element[1];
        if(!result.length || result[result.length-1] < value)
            result.push(value);

        if(result.length == k) {
            break;
        }
        var indexArrStatus = indexArr[index];
        if(indexArrStatus[0] <= indexArrStatus[1]) {
            var val = matrix[index][indexArrStatus[0]];
            minHeap.push([val, index]);
            indexArrStatus[0] = indexArrStatus[0] + 1;
        }
    }

    return result.pop();
};
  
  
console.log(`Kth smallest number is: ${find_Kth_smallest([[2, 6, 8], [3, 7, 10], [5, 8, 11]], 5)}`)
  