var Heap = require("collections/heap");

const find_k_frequent_numbers = function(nums, k) {
    var minHeap = new Heap([], null, (a,b) => b[1]-a[1]);
    var topNumbers = [];
    var map = {};
    
    nums.forEach(element => {
        map[element] = (map[element] || 0) + 1;
    });

    Object.keys(map).forEach(key => {
        var frequency = map[key];
        if(minHeap.length < k ) {
            minHeap.push([key, frequency]);
        }
        if(frequency > minHeap.peek()[1]) {
            minHeap.pop();
            minHeap.push([key, frequency])
        }
    });

    while(minHeap.length) {
        topNumbers.push(minHeap.pop()[0]);
    }

    return topNumbers;
};
  
  
console.log(`Here are the K frequent numbers: ${find_k_frequent_numbers([1, 3, 5, 12, 11, 12, 11], 2)}`)
console.log(`Here are the K frequent numbers: ${find_k_frequent_numbers([5, 12, 11, 3, 11], 2)}`)
  