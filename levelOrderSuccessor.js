const Deque = require('collections/deque');
class TreeNode {

    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null; 
    }
  };
  
  
const find_successor = function(root, key) {
    // TODO: Write your code here
    var q = new Deque();
    q.push(root);
    var successor = false;
    while(q.length) {
        var qLen = q.length;
        for(var i = 0; i < qLen; i++) {
            var val = q.shift();
            if(successor) {
                return {val : val.value};
            }
            if(val.value == key) {
                successor = true;
            }
            if(val.left)
                q.push(val.left);
            if(val.right)
                q.push(val.right);
        }
    }

    return {val : null};
};
  
  
  var root = new TreeNode(12)
  root.left = new TreeNode(7)
  root.right = new TreeNode(1)
  root.left.left = new TreeNode(9)
  root.right.left = new TreeNode(10)
  root.right.right = new TreeNode(5)
  result = find_successor(root, 12)
  if (result != null)
    console.log(result.val)
  result = find_successor(root, 9)
  if (result != null)
    console.log(result.val)
  