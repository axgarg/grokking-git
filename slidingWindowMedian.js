var Heap = require("collections/heap");
class SlidingWindowMedian {
    constructor() {
        this.maxHeap = new Heap([], null, ((a,b) => a - b));
        this.minHeap = new Heap([], null, ((a,b) => b - a));
    }

    insert_into_heap(num) {
        if(this.maxHeap.length == 0 || this.maxHeap.peek() >= num) {
            this.maxHeap.push(num);
        } else {
            this.minHeap.push(num);
        }

        if(this.maxHeap.length > this.minHeap.length + 1) {
            this.minHeap.push(this.maxHeap.pop());
        } else if(this.minHeap.length > this.maxHeap.length) {
            this.maxHeap.push(this.minHeap.pop());
        }
    }

    delete_into_heap(num) {
        if(this.maxHeap.peek() >= num) {
            this.maxHeap.delete(num);
        } else {
            this.minHeap.delete(num);
        }

        if(this.maxHeap.length > this.minHeap.length + 1) {
            this.minHeap.push(this.maxHeap.pop());
        } else if(this.minHeap.length > this.maxHeap.length) {
            this.maxHeap.push(this.minHeap.pop());
        }
    }

    get_median() {
        if((this.maxHeap.length + this.minHeap.length)%2 == 0) {
            return (this.maxHeap.peek() + this.minHeap.peek())/2;
        } else {
            return this.maxHeap.peek();
        }
    }



    find_sliding_window_median(nums, k) {
        var result = [];
        // TODO: Write your code here
        var start = 0;
        var len = nums.length;
        var index = 0;
        while(start < len) { 
            this.insert_into_heap(nums[start]);
            var heapLen = this.maxHeap.length + this.minHeap.length;
            if(heapLen == k) {
                result.push(this.get_median());
                this.delete_into_heap(nums[index]);
                index++;
            }
            start++;
        }

        return result;
    }
};

var slidingWindowMedian = new SlidingWindowMedian()
result = slidingWindowMedian.find_sliding_window_median([1, 2, -1, 3, 5], 2)
console.log(`Sliding window medians are: ${result}`)

slidingWindowMedian = new SlidingWindowMedian()
result = slidingWindowMedian.find_sliding_window_median([1, 2, -1, 3, 5], 3)
console.log(`Sliding window medians are: ${result}`)
  