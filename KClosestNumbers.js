var Heap = require("collections/heap");

const find_closest_elements = function(arr, K, X) {
    var maxHeap = new Heap([], null, (a,b) => a[1]-b[1]);




    for(var  i = 0; i < arr.length; i++) {
        var val = arr[i];
        var distance = Math.abs(val-X);

        if(maxHeap.length < K) {
            maxHeap.push([val, distance]);
        } else {
            if(maxHeap.peek()[1] > distance) {
                maxHeap.pop();
                maxHeap.push([val, distance]);
            }
        }
    }

    // TODO: Write your code here
    var res = [];
    while(maxHeap.length) {
        res.push(maxHeap.pop()[0]);
    }
    return res;
};

function binary_search(arr, target) {
    
    var low = 0;
    var high = arr.length - 1;

    while(low < high) {
        var mid = Math.floor((low+high)/2);
        if(arr[mid] == target) {
            return mid;
        } else if (arr[mid] > target) {
            high = mid-1;
        } else {
            low = mid+1;
        }
    }
}
  
  
console.log(`'K' closest numbers to 'X' are: ${find_closest_elements([5, 6, 7, 8, 9], 3, 7)}`)
console.log(`'K' closest numbers to 'X' are: ${find_closest_elements([2, 4, 5, 6, 9], 3, 6)}`)
console.log(`'K' closest numbers to 'X' are: ${find_closest_elements([2, 4, 5, 6, 9], 3, 10)}`)
  