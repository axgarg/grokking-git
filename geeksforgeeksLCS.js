

function LCS(nums) {

    dp = Array(nums.length);
    var t = lcs_recurse(nums, -1, 0, dp);
    console.log(dp);
    return t;
}



function lcs_recurse(nums, prev, index) {

    if(index == nums.length) {
        return 0;
    }

    if(dp[index])
        return dp[index];

    var first = 0;
    var second = 0;

    if(nums[index] > prev) {
        first = 1 + lcs_recurse(nums, nums[index], index+1, dp);    
    } 
    second = lcs_recurse(nums, prev, index+1, dp);    

    dp[index] = Math.max(first, second); 
    return dp[index];

}

console.log(LCS([10,8,3,7,9,1]));
console.log(LCS([0,8,4,12,2,10,6,14,1,9,5,13,3,11,7]));